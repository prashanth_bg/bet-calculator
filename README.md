# Bet Calculator

### How to run
Install Python 2.7x from https://www.python.org/downloads/ 
clone the repo with 
```
git clone git@bitbucket.org:prashanth_bg/bet-calculator.git
```
Run the below command from a Linux shell or if on windows, from a git bash shell
```
 cat inputBets | python calc_bet.py
```
This will print the results on the console. The input file is inputBets which is read from stdin.

Results can also be seen here in the CI pipeline here by clicking on the command below the Build setup in the Logs section:
https://bitbucket.org/prashanth_bg/bet-calculator/addon/pipelines/home#!/results/2

This is a little program written in Python. Why Python ?
1. Best suited for parsing raw input data to structured data for manipulation.
2. Very less code, advantage of functional programming, list comprehension. 

