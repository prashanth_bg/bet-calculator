#!/usr/bin/python2
from __future__ import division
import sys
import itertools
import copy 

#constants
WINNING_COMMISSION_PCT = 15
PLACE_COMMISSION_PCT = 12
EXACTA_COMMISSION_PCT = 18 

#globals to hold bets
win = []
place = []
exacta = []
result = [] 

def init():
    global result, win, place, exacta, key
    data = read_input()
    result = data.pop().split(':')[1:]
    for key, value in itertools.groupby(data, key=lambda x:x.split(':')[1]):
        extracts = [tuple(val.split(':')[2:4]) for val in list(value)]
        if (key == 'W'):
            win = sanitize_numbers(extracts)
        elif (key == 'P'):
            place = sanitize_numbers(extracts)
        elif (key == 'E'):
            exacta = copy.deepcopy(extracts)

    win_dividents, winner = calculate_winning_divident()
    print(get_result_string('Win', winner, win_dividents))

    place_dividents = calculate_place_dividents()
    for key in place_dividents:
        print(get_result_string('Place', key, place_dividents[key]))

    exacta_dividents, winners = calculate_exacta_dividents()
    print(get_result_string('Exacta', winners, exacta_dividents))
    
def get_result_string(bet, winner, divident):
    return bet + ':' + str(winner) + ':$' + str(divident)

def read_input():
    return [item.strip() for item in sys.stdin.readlines()]

def sanitize_numbers(data):
    return [map(int, x) for x in copy.deepcopy(data)]

def compute_sum(list):
    return reduce(lambda x,y : x+y[1], list, 0)

def discount_commission(amount, commission):
    return amount - (amount * commission / 100)

def calculate_winning_divident():
    global result
    winning_horse = int(result[0])
    total_winning_bets = compute_sum(win)
    bets_on_winning_horse = [i for i in win if i[0] == winning_horse]
    total_bets_on_winning_horse = sum(x[1] for x in bets_on_winning_horse)
    wins_per_dollar = round((discount_commission(total_winning_bets, WINNING_COMMISSION_PCT ))/total_bets_on_winning_horse, 2)
    return winning_horse, wins_per_dollar

def get_place_list(main_list, sub_list, amount, index):
    filtered_list = [i for i in main_list if i[0] == sub_list[index]]
    return round(amount / sum([x[1] for x in filtered_list]), 2)

def calculate_place_dividents():
    winning_place_horses = [int(x) for x in result]
    total_place_bets = compute_sum(place)    
    place_bets_on_winning_horse = [i for i in place if i[0] in winning_place_horses]
    total_place_bets_on_winning_horse = sum(x[1] for x in place_bets_on_winning_horse)
    total_place_bets_split = discount_commission(total_place_bets, PLACE_COMMISSION_PCT) / 3
    wins_per_dollar_place_1 = get_place_list(place_bets_on_winning_horse, winning_place_horses, total_place_bets_split, 0)
    wins_per_dollar_place_2 = get_place_list(place_bets_on_winning_horse, winning_place_horses, total_place_bets_split, 1)
    wins_per_dollar_place_3 = get_place_list(place_bets_on_winning_horse, winning_place_horses, total_place_bets_split, 2)
    return {
            winning_place_horses[0] : wins_per_dollar_place_1,
            winning_place_horses[1] : wins_per_dollar_place_2,
            winning_place_horses[2] : wins_per_dollar_place_3
           } 

def calculate_exacta_dividents():
    total_exacta_bets = reduce(lambda x,y : x+int(y[1]), exacta, 0) 
    exacta_bets_on_winning_horse = [i for i in exacta if list(i[0].split(',')) == result[0:2]]
    total_exacta_bets_after_commission = discount_commission(total_exacta_bets, EXACTA_COMMISSION_PCT)
    wins_per_dollar_exacta = round(total_exacta_bets_after_commission / sum([int(x[1]) for x in exacta_bets_on_winning_horse]), 2)
    return wins_per_dollar_exacta, exacta_bets_on_winning_horse[0][0]

if __name__ == '__main__':
    init()

